﻿using System;

namespace AddTwoDigits
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Hello World!");
        }

        int addTwoDigits(int n) {
            return ((n % 10) + (n / 10));
        }

    }
}
